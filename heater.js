class Header extends HTMLElement {
    connectedCallback() {
      this.innerHTML = 
      `
      
<div class="topstrip p-0">
<div class="mr-5">
<div class="row">
<div class="col-md-7 col-sm-12">
  <div class="topstrip-menu">
    <div class="topstrip-menu">
      <ul id="menu-top_strip_menu" class="menu">
        <li id="menu-item-134" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134"><a href="circular.html">Circulars</a></li>
        <li id="menu-item-131" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-131"><a target="_blank" href="https://mail.icar.gov.in/owa/auth/logon.aspx?replaceCurrent=1&amp;url=https%3a%2f%2fmail.icar.gov.in%2fowa%2f">ICAR-Email</a></li>
        <li id="menu-item-1364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1364"><a href="http://icarerp.iasri.res.in">ICAR-ERP</a></li>
        <li id="menu-item-1382" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1382"><a href="http://hypm.iasri.res.in/">HYPM</a></li>
        <li id="menu-item-1381" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1381"><a href="http://pimsicar.iasri.res.in/">PIMS</a></li>
        <li id="menu-item-1380" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1380"><a href="https://pfms.nic.in/NewDefaultHome.aspx">PFMS</a></li>
        <li id="menu-item-1379" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1379"><a href="https://gem.gov.in/">GeM</a></li>
        <li id="menu-item-1378" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1378"><a href="https://eprocure.gov.in/eprocure/app">E-Procurement</a></li>
        <li id="menu-item-1821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1821"><a href="https://dsrindb.attendance.gov.in/">Aadhar-Biometric</a></li>
      <!--  <li id="menu-item-1821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1821"><a href="screenreader.html">Screen Reader Access</a></li>-->
      </ul>
    </div>					
  </div>       
</div>

<!-- model popup -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content justify-center">
            <div class="modal-header color4">
            <h1 class="model-title text-center w-100"> Covid Guidelines</h1>
            <button type="button" class="close" data-dismiss="modal">&times;</button>              
            </div>
            <div class="modal-body color2">
            
                
            <img class="w-100 " src="slides/covidforsite.jpg" alt="covid Guidelines"><br>
      <!--  <marquee onmouseover="this.stop()" onmouseout="this.start()"><font color="Maroon" size="3"><b> Vigilance Awareness Week Celebrated during 27 Oct-2 Nov 2020. </b>&nbsp;&nbsp;&nbsp; <b><a href="pdfdoc/Adv_TechPost_2020.pdf" target="iframe_a">Filling up 03 posts of Technical Assistant (T-3) Category-II (Functional Group –Field farm-2 and Lab technician -1) Group C Level-5 on inter-institutional transfer basis </a></b>

</marquee>-->
                
            </div>
        </div>
    </div>
</div>



<div class="col-md-5 col-sm-12">
<div class="topstrip-right">	
<span class="ab"><a href="./stiemap.html"><img src="top-search.png" alt="search img" /></a></span>
<span class="ab"><a href="./screenreader.html"><img src="screenreader.png" alt="Screen Reader" /></a></span>
      


    

         <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
  <a rel="alternate" class="sellang" hreflang="hi" href="../newwebsitehindi/card_indexH.html">English</a></button>
        <ul class="dropdown-menu language_bar_chooser ">
            <li class="active" id="setlang">
    
            </li>
    
            <li>
                <a rel="alternate" class="sellang" hreflang="hi" href="../newwebsitehindi/card_indexH.html">हिन्दी</a>
            </li>
                            </ul>
    </div>
    

   

</div><!-- topstrip-right -->

</div><!-- col-6 -->							
</div><!-- row -->
</div><!-- container -->
</div>

<header class="headertab">
<div class=" ">
<div class="row tranparency">
<div class="col-md-2 p-0 ">
<img class="logo3 p-4"    src="icarlogo.png"  alt="ICAR Logo">

</div>        
<div class="col-md-8 col-lg-8 p-0">
<nav class=" navbar-expand-md navbar justify-content-center  ">
 <div class="nationInfo">
   <!-- <img src="text-logo1.png" alt="ICAR-Indian-Institute-of-Soybean-Research-Indore"/> -->
   <center>
    <b><h3>भा.कृ.अनु.प. - भारतीय सोयाबीन अनुसंधान संस्थान</h3></b>
    <b><h3 id="hline">ICAR-INDIAN INSTITUTE OF SOYBEAN RESEARCH</h3></b>
    <a href="pdfdoc/ISOcerti.pdf" id="iso">(ISO 9001:2015 Certified Organization)</a>
   </center>
 </div>


  

</nav>


</div>
<div class="col-md-2 p-0 ">
<img class="logo1"    src="./right_logo.webp"  alt="ICAR Logo">

</div> 
</div>

</div>

</header>

<nav class="navbar navbar-expand-lg navbar-dark" id="default2">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center " id="main_nav">
    
    <ul class="navbar-nav">
    <li class="nav-item"> <a class="nav-link" href="card_index.html"> Home </a> </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> About Us </a><hr>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="AboutUs.html"> About Institute &raquo </a><hr>
             <ul class="submenu dropdown-menu">
                <li><a class="dropdown-item" href="AboutUs.html"> History</a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Mandate</a></li><hr>
                <li><a class="dropdown-item" href="significant_achievement.html"> Significant achievements </a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Thrust Areas </a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Collaborations </a></li>
             </ul>
          </li>
          <li><a class="dropdown-item" href="instituteprofile.html">Institute Profile </a></li><hr> 
          <li><a class="dropdown-item" href="scienctific_staff.html"> Staff </a></li><hr>          
          <li><a class="dropdown-item" href="organization_chart.html">Organization Chart </a></li><hr>
          <li><a class="dropdown-item" href="./farm.html"> Facilities &raquo </a><hr>
             <ul class="submenu dropdown-menu">
                <li><a class="dropdown-item" href="./farm.html"> Farm</a></li><hr>
                <li><a class="dropdown-item" href="./researchlabs.html"> Research Labs</a></li><hr>
                <li><a class="dropdown-item" href="./instrumentfacility.html"> Instrument Facility</a></li><hr>
                <li><a class="dropdown-item" href="./library.html"> Library</a></li><hr>                
                <li><a class="dropdown-item" href="tofuplant.html"> Tofu Plant </a></li><hr>
                <li><a class="dropdown-item" href="./guesthouse.html">  Guest House </a></li><hr>
             </ul>
          </li>
          <li><a class="dropdown-item" href="./cadrestrenght.html">Cadre Strength </a></li><hr>
          <li><a class="dropdown-item" href="institutecommittee.html"> Committies &raquo </a><hr>
             <ul class="submenu dropdown-menu">
          <li><a class="dropdown-item" href="./institutecommittee.html">Institute Committee </a></li><hr>          
          <li><a class="dropdown-item" href="./institutecommittee.html">IMC</a></li><hr>
          <li><a class="dropdown-item" href="./institutecommittee.html">RAC</a></li><hr>
          <li><a class="dropdown-item" href="./institutecommittee.html">QRT</a></li>
          </ul>
          <li><a class="dropdown-item" href="IPR.html">ITMU/Commercial Tech.</a></li><hr>
          <li><a class="dropdown-item" href="formerdirectors.html">Former Directors </a></li>
        </ul>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Public Information </a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="RTI.html"> RTI </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/citizencharter.pdf">Citizen Charter </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/strategicplan.pdf">Strategic Plan </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/pentioner.pdf">For Pensioners </a></li>
          
        </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Farmer's Corner </a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="./goodagripractices.html"> Good Agricultural Practices </a></li><hr>
        <li><a class="dropdown-item" href="./varieties.html">Varieties </a></li><hr>
        <li><a class="dropdown-item" href="./Advisory.html"> Advisories </a></li><hr>
        <li><a class="dropdown-item" href="./fooduses.html"> Food Uses </a></li><hr>
      <!--  <li><a class="dropdown-item" href="#">SCSP </a></li><hr>
        <li><a class="dropdown-item" href="#">NEH </a></li><hr>
        <li><a class="dropdown-item" href="#">TSP</a></li>-->
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Awards </a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="./institute.html"> Institute </a></li>
        <li><a class="dropdown-item" href="./individualawards.html"> Individuals </a></li>        
      </ul>
    </li>
    <li class="nav-item"> <a class="nav-link" href="aicrps_new.html"> AICRPS </a> </li>    
    <li class="nav-item"> <a class="nav-link" href="faq.html"> FAQ </a></li>
    <li class="nav-item"> <a class="nav-link" href="contactus.html"> Contact Us</a></li>
    <li class="nav-item"> <a class="nav-link" href="../newwebsitehindi/card_indexH.html">हिन्दी </a></li>  
    <li class="nav-item"> <a class="nav-link" href="https://play.google.com/store/apps/details?id=com.icar.soyainfo&hl=en">Mobile App-Soybean Gyan</a></li>  
    <ul class="social-network social-circle">
        <li><a href="https://www.youtube.com/channel/UCNdY5AsfPZqsCO8IxkAuSyQ?view_as=subscriber" class="icoLinkedin" title="Youtube"><i class="fa fa-youtube"></i></a></li>
        <li><a href="https://www.facebook.com/ICAR-Indian-Institute-of-Soybean-Research-Indore-507415769433553/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/icarindia/status/1292312168931581952?s=24" class="icotwitter" title="icotwitter"><i class="fa fa-twitter"></i></a></li>
        
    </ul>
    </ul>
    
    </div> <!-- navbar-collapse.// -->
    </nav>
      `
     }
  }

  class AICRPSHeader extends HTMLElement {
    connectedCallback() {
      this.innerHTML=`
      <nav class="navbar navbar-expand-lg navbar-dark" id="default2">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center " id="main_nav">
    
    <ul class="navbar-nav">
    <li class="nav-item"> <a class="nav-link" href="card_index.html"> Home </a> </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> About Us </a><hr>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="AboutUs.html"> About Institute &raquo </a><hr>
             <ul class="submenu dropdown-menu">
                <li><a class="dropdown-item" href="AboutUs.html"> History</a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Mandate</a></li><hr>
                <li><a class="dropdown-item" href="significant_achievement.html"> Significant achievements </a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Thrust Areas </a></li><hr>
                <li><a class="dropdown-item" href="AboutUs.html"> Collaborations </a></li>
             </ul>
          </li>
          <li><a class="dropdown-item" href="instituteprofile.html">Institute Profile </a></li><hr> 
          <li><a class="dropdown-item" href="scienctific_staff.html"> Staff </a></li><hr>          
          <li><a class="dropdown-item" href="organization_chart.html">Organization Chart </a></li><hr>
          <li><a class="dropdown-item" href="./farm.html"> Facilities &raquo </a><hr>
             <ul class="submenu dropdown-menu">
                <li><a class="dropdown-item" href="./farm.html"> Farm</a></li><hr>
                <li><a class="dropdown-item" href="./researchlabs.html"> Research Labs</a></li><hr>
                <li><a class="dropdown-item" href="./instrumentfacility.html"> Instrument Facility</a></li><hr>
                <li><a class="dropdown-item" href="./library.html"> Library</a></li><hr>                
                <li><a class="dropdown-item" href="tofuplant.html"> Tofu Plant </a></li><hr>
                <li><a class="dropdown-item" href="./guesthouse.html">  Guest House </a></li><hr>
             </ul>
          </li>
          <li><a class="dropdown-item" href="./cadrestrenght.html">Cadre Strength </a></li><hr>
          <li><a class="dropdown-item" href="institutecommittee.html"> Committies &raquo </a><hr>
             <ul class="submenu dropdown-menu">
          <li><a class="dropdown-item" href="./institutecommittee.html">Institute Committee </a></li><hr>          
          <li><a class="dropdown-item" href="./institutecommittee.html">IMC</a></li><hr>
          <li><a class="dropdown-item" href="./institutecommittee.html">RAC</a></li><hr>
          <li><a class="dropdown-item" href="./institutecommittee.html">QRT</a></li>
          </ul>
          <li><a class="dropdown-item" href="IPR.html">ITMU/Commercial Tech.</a></li><hr>
          <li><a class="dropdown-item" href="formerdirectors.html">Former Directors </a></li>
        </ul>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Public Information </a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="RTI.html"> RTI </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/citizencharter.pdf">Citizen Charter </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/strategicplan.pdf">Strategic Plan </a></li><hr>
          <li><a class="dropdown-item" href="pdfdoc/pentioner.pdf">For Pensioners </a></li>
          
        </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Farmer's Corner </a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="./goodagripractices.html"> Good Agricultural Practices </a></li><hr>
        <li><a class="dropdown-item" href="./varieties.html">Varieties </a></li><hr>
        <li><a class="dropdown-item" href="./Advisory.html"> Advisories </a></li><hr>
        <li><a class="dropdown-item" href="./fooduses.html"> Food Uses </a></li><hr>
      <!--  <li><a class="dropdown-item" href="#">SCSP </a></li><hr>
        <li><a class="dropdown-item" href="#">NEH </a></li><hr>
        <li><a class="dropdown-item" href="#">TSP</a></li>-->
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Awards </a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="./institute.html"> Institute </a></li>
        <li><a class="dropdown-item" href="./individualawards.html"> Individuals </a></li>        
      </ul>
    </li>
    <li class="nav-item"> <a class="nav-link" href="aicrps_new.html"> AICRPS </a> </li>    
    <li class="nav-item"> <a class="nav-link" href="faq.html"> FAQ </a></li>
    <li class="nav-item"> <a class="nav-link" href="contactus.html"> Contact Us</a></li>
    <li class="nav-item"> <a class="nav-link" href="../newwebsitehindi/card_indexH.html">हिन्दी </a></li>  
    <li class="nav-item"> <a class="nav-link" href="https://play.google.com/store/apps/details?id=com.icar.soyainfo&hl=en">Mobile App-Soybean Gyan</a></li>  
    <ul class="social-network social-circle">
        <li><a href="https://www.youtube.com/channel/UCNdY5AsfPZqsCO8IxkAuSyQ?view_as=subscriber" class="icoLinkedin" title="Youtube"><i class="fa fa-youtube"></i></a></li>
        <li><a href="https://www.facebook.com/ICAR-Indian-Institute-of-Soybean-Research-Indore-507415769433553/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/icarindia/status/1292312168931581952?s=24" class="icotwitter" title="icotwitter"><i class="fa fa-twitter"></i></a></li>
        
    </ul>
    </ul>
    
    </div> <!-- navbar-collapse.// -->
    </nav>
    `
    }
  }

  class News extends HTMLElement {
    connectedCallback() {
      this.innerHTML = `
      <div class="container">
			<h5 class="text-center fonttypeheading mt-5"><strong class="font-weight-light">News and Events</strong></h5>
			<br>
			<div class="side-margin color4 p-3">
				<marquee behavior="scroll" direction="up" height="180" width="100%" scrollamount="1" scrolldelay="50" truespeed="true" onmouseover="this.stop()" onmouseout="this.start()">
					<ul>
						<li><a href="../pdfdoc/FormerDirectorDrBhatt.pdf"> Sad demise of Dr P S Bhatnagar, Founder Director of Institute </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/Gandhi_Jayanti_Celebration_2020.pdf"> Gandhi Jayanti Celebration 2020 </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/PressNote-BrainstormingMeet_25_sep_2020_Eng.pdf"> National Oilseed Brainstorming Meet Day-3 25 Sept 2020 </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/PressNote-BrainstormingMeet_24_sep_2020.pdf"> National Oilseed Brainstorming Meet Day-2 24 Sept 2020 </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/Press Note2-Brainstorming-23sept2020.pdf"> National Oilseed Brainstorming Meet Day-1 23 Sept 2020  </a>
						</li>
						<br>
						<li><a href="../pdfdoc/PressNote_BrainStormingMeet_21_sep_2020.pdf"> National Oilseed Brainstorming Meet to be organized on 23-25 Sept 2020 </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/HindiPakhwadaSamapan2020.pdf"> Hindi Pakhwada organized from 1-14 Sep 2020 </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/KotaMeet5Sep2020.pdf"> Virtual Meet held on 5 Sep 2020 for insect problem in Rajasthan.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/KVKMeet31Aug2020.pdf"> Web meeting with KVKs held on 31 Aug 2020 for status of soybean yield losses.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/IMCmeetnews.pdf"> 30th IMC meeting held virtually on 14 Aug 2020.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/DVSinghpressnote.pdf">  Dr DV Singh, Principal Scientist got NASI-ICAR award 2019. </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/varietyrelease.pdf">  Nine Soybean varieties identified for release. </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/yogaday2020.pdf">  International Yoga Day Celebrated using digital platform on 21 June 2020. </a> 
						</li>
						<br>
						<li><a href="../pdfdoc/34thIRCNews.pdf"> Virtual IRC held on 16-17 June 2020.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/webinar-1-2020.pdf">  Webinar on field preparation organized on 11 June 2020.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/environmentday2020.pdf">  Environment day celebrated on 05 June 2020.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/Farewell.pdf">  Virtual farewell of Director on superannuation held on 30 May 2020.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/50AGMReport.pdf">   50th Virtual AGM of AICRPS held during 20 May 2020 using digital platform.</a> 
						</li>
						<br>
						<li><a href="../pdfdoc/NRC142News.pdf">   NRC 142 free from KTI and lipoxygenase 2 developed.</a> 
						</li>
						<br>
					</ul>
				</marquee>
			</div>
    </div>
    `
    }
  }
  
class Footer extends HTMLElement {
connectedCallback() {
    this.innerHTML = `
    <div class="home-footer pt-2 bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <marquee direction="left" behavior="scroll" scrolldelay=120 onmouseover="this.stop();"  onmouseout="this.start();" > 
            <a href="#"><img src="./marquee_logos/digi-india-logo.jpg" alt="#">  </a>&nbsp;&nbsp;
            <img id="footermarquee" src="./marquee_logos/swacha-barat-logo.jpg" alt="#">  &nbsp;&nbsp;
            <img id="footermarquee" src="./marquee_logos/india-gov-logo.jpg" alt="#">  &nbsp;&nbsp;
            <img id="footermarquee" src="./marquee_logos/make-in-india-logo.jpg" alt="#">  &nbsp;&nbsp;
            
            <img  id="footermarquee" src="https://icar.org.in/sites/default/files/download%20%283%29.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/pmnrf_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee"  src="https://icar.org.in/sites/default/files/pg-portal_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee"  src="https://icar.org.in/sites/default/files/GoI-directory_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/e-gazette_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/rti-en_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/data-gov_0_1.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/eci_0.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/data-gov.png" alt="">  &nbsp;&nbsp;
            <img id="footermarquee" src="https://icar.org.in/sites/default/files/cashlees_india_0.png" alt="">  &nbsp;&nbsp;
          </marquee>
        </div>
      </div>
    </div>
  </div>
  <footer class="mainfooter bg-dark" role="contentinfo">
    <div class="footer-middle">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <!--Column1-->
          <div class="footer-pad">
            <h4>Transfer of Technology</h4>
            <ul class="list-styled pl-4">
              <li><a href="extensionact.html">Extension Activities</a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <!--Column1-->
          <div class="footer-pad">
            <h4>Downloads</h4>
            <ul class="list-styled pl-4">

              <li><a href="statistics.html">Statistics</a></li>            
              <li><a href="pdfdoc/holidaylist2020.pdf">Holiday-List</a></li>
              <li><a href="pdfdoc/AllForms.pdf">Institute Forms</a></li>
              <li><a href="pdfdoc/SeedproductAgreement.pdf"><b>Seed Production Contract</b></a> </li>
              <li><a href="pdfdoc/Guidelines For Registration of Plant Germplasm (Revised 2014).pdf"><b>Guidelines For Registration of Plant Germplasm</b></a> </li>
              <li><a href="otherforms.html">Others</a></li>

            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <!--Column1-->
          <div class="footer-pad">
            <h4 class="new">Related Sites</h4>
              <div class="row container">
                <div class="col-sm-7 ">
                  <ul class="list-styled pl-4">
                    <li><a href="http://www.icar.org.in/">ICAR</a></li>
                    <li><a href="http://jgateplus.com/search/journalFinder/">CeRA</a></li>
                    <li><a href="http://www.ssrd.co.in/">SSRD</a></li>
                    <li><a href="http://icarerp.iasri.res.in">FMS-MIS</a></li>                  
                    <li><a href="http://dare.nic.in">DARE</a></li>
                    <li><a href="http://agricoop.nic.in">DAC</a></li>
                    <li><a href="https://krishi.icar.gov.in/">KRISHI</a></li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <h4>Services</h4>
              <ul>
               <li><a href='./consultancyservice.html'>Consultancy</a></li>
              <!-- <li><a href='#'>CPC</a></li>
               <li><a href='IPR.html'>ITMU</a></li>
               <li><a href='#'>Breeder Seed</a></li>-->
  
  </a></li>
              </ul>				
      </div>
      </div>
    <div class="row">
      <div class="col-md-12 copy pb-0 mb-0">
        <p class="text-center pb-0 mb-0">&copy; Copyright 2020 -ICAR-Indian Institute of Soybean Research.  All Rights Reserved.</p>
        <p class="text-center pb-0 mb-0">Hosted on: 11 Dec 2020<br>Updated on: 11 Dec 2020</p>
       <p class="text-center ">
        <a href="./contactus.html">Contact us | </a>
        <a href="disclaimer.html">Disclaimer | </a>
        <a href="./feedback.html">Feedback | </a>
        <a href="card_index.html">Home | </a>
        <a href="linkingpolicy.html">Linking Policy |</a>
        <a href="privacypolicy.html"></a>
        <p class="text-white text-center ">Visitors Arrived: <a id="CounterVisitor"></a></p>
       </p>
        </div>
  
      </div>
    </div>
  
  
    </div>
    </div>
  </footer>   
    `;
  }
}
  
class SideNav extends HTMLElement {
  connectedCallback() {
    this.innerHTML= `
    <div class="">
    <!-- partial:index.partial.html -->
 <a href="instituteprofile.html" class="sidenava">
     <div id="btn1" class="button">
       <h1>Institute Profile</h1>
     </div>
   </a>
   
   <a href="organization_chart.html"  class="sidenava">
     <div id="btn2" class="button">
       <h1>Organization Chart</h1>
     </div>
   </a>
   
   <a href="./cadrestrenght.html" class="sidenava">
     <div id="btn3" class="button">
       <h1>Cadre Strength</h1>
     </div>
   </a>
   <a href="scienctific_staff.html" class="sidenava">
     <div id="btn4" class="button">
       <h1>Staff</h1>
     </div>
   </a>
   <a href="significant_achievement.html" class="sidenava">
     <div id="btn5" class="button">
       <h1>Achievements</h1>
     </div>
   </a>
   <a href="guesthouse.html" class="sidenava">
   <div id="btn6" class="button">
     <h1>Facilities</h1>
   </div>
 </a>
 <a href="institutecommittee.html" class="sidenava">
   <div id="btn7" class="button">
     <h1>Committees</h1>
   </div>
 </a>
 <a href="formerdirectors.html" class="sidenava">
   <div id="btn8" class="button">
     <h1>Former Directors</h1>
   </div>
 </a>
   <!-- partial -->
  </div>
    `
  }
}

class FacilityNav extends HTMLElement {
  connectedCallback() {
    this.innerHTML= `
    <div class="">
    <!-- partial:index.partial.html -->
 <a href="farm.html" class="sidenava">
     <div id="btn1" class="button">
       <h1>Farm</h1>
     </div>
   </a>
   
   <a href="researchlabs.html"  class="sidenava">
     <div id="btn2" class="button">
       <h1>Research Labs</h1>
     </div>
   </a>
   <a href="instrumentfacility.html" class="sidenava">
     <div id="btn3" class="button">
       <h1>Instrument Facility</h1>
     </div>
   </a>
   <a href="library.html" class="sidenava">
     <div id="btn4" class="button">
       <h1>Library</h1>
     </div>
   </a>
   <a href="tofuplant.html" class="sidenava">
     <div id="btn5" class="button">
       <h1>Tofu Plant</h1>
     </div>
   </a>
   <a href="guesthouse.html" class="sidenava">
     <div id="btn6" class="button">
       <h1>Guest House</h1>
     </div>
   </a>
   
   
  </div>
    `
  }
}

class NavDrawerF extends HTMLElement {
  connectedCallback() {
    this.innerHTML= `
    <div class="">
    <!-- partial:index.partial.html -->
 <a href="goodagripractices.html" class="sidenava">
     <div id="btn1" class="button">
       <h1>Agri Practices</h1>
     </div>
   </a>
   
   <a href="varieties.html"  class="sidenava">
     <div id="btn2" class="button">
       <h1>Varieties</h1>
     </div>
   </a>
   
   <a href="Advisory.html" class="sidenava">
     <div id="btn3" class="button">
       <h1>Advisories</h1>
     </div>
   </a>
   <a href="#" class="sidenava">
     <div id="btn4" class="button">
       <h1>Food Uses</h1>
     </div>
   </a>
   <a href="#" class="sidenava">
     <div id="btn5" class="button">
       <h1>SCSP</h1>
     </div>
   </a>
   <a href="#" class="sidenava">
     <div id="btn6" class="button">
       <h1>NEW</h1>
     </div>
   </a>
   <a href="#" class="sidenava">
   <div id="btn7" class="button">
     <h1>TSP</h1>
   </div>
 </a>

   <!-- partial -->
  </div>
    `
  }
}

customElements.define('nav-drawer', NavDrawerF);

  

customElements.define('main-sidenav',SideNav);
customElements.define('main-facilitynav',FacilityNav);
customElements.define('main-header', Header);
  

customElements.define('main-aicprsheader', AICRPSHeader);
customElements.define('main-footer', Footer);
customElements.define('main-news', News);
  